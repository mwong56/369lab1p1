import org.json.JSONObject;

/**
 * Created by Michael Wong and Timothy Chu
 * Class to hold and print Action Special.
 */
public class ActionSpecial extends BaseObject {

  private static final String actionType = "SpecialMove";

  private int actionNumber;
  private SpecialMoveName move;
  private int pointsAdded;
  private int points;

  public ActionSpecial(int actionNumber, SpecialMoveName move, int points) {
    this.actionNumber = actionNumber;
    this.move = move;
    this.pointsAdded = random.nextInt(41) - 20;
    this.points = points + pointsAdded;
  }


  public int getActionNumber() {
    return actionNumber;
  }

  public int getPoints() {
    return points;
  }

  public JSONObject toJSONObject() {
    JSONObject obj = new JSONObject();
    obj.put("actionNumber", actionNumber);
    obj.put("actionType", actionType);
    obj.put("move", move);
    obj.put("pointsAdded", pointsAdded);
    obj.put("points", points);
    return obj;
  }

  public enum SpecialMoveName {
    Shuffle, Clear, Invert, Rotate
  }
}
