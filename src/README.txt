LAB 1 PART 1
Author: tchu01 && mwong56

beFuddledGen:
	- To compile: javac -cp json-20131018.jar *.java
	- To run:
		- First argument is number of logs
		- Second argument is file name where the logs will be printed.
		- Example:
		- java -cp .;json-20131018.jar beFuddledGen 100 hello.txt
	- General assumptions:
		- Winning a game is 50/50 through random number
		- Total number of games that is being played at any given time = 50/50
		- Special move chance percentages:
			- Shuffle = 33%
			- Invert = 25%
			- Clear = 23%
			- Rotate = 19%
		- End game percentages:
			- If move count is 9-40 => 20%
			- 40-50 = 50%
			- 51-70 = 20%
			- 70-99 = 10%
			- 100 = 100%
	- Program requires roughly 500 MB of memory to run. 
		- Can be more efficient by reducing # of simultaneous games being played.
		
		