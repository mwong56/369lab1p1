/**
 * Created by Michael Wong and Timothy Chu
 * Class to hold and print Action Start.
 */
class ActionStart {

  private static final int actionNumber = 1;
  private static final String actionType = "GameStart";

  public ActionStart() {
  }

  public org.json.JSONObject toJSONObject() {
    org.json.JSONObject object = new org.json.JSONObject();
    object.put("actionNumber", actionNumber);
    object.put("actionType", actionType);
    return object;
  }
}
