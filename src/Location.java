import org.json.JSONObject;

import java.util.Random;

/**
 * Created by Michael Wong and Timothy Chu
 * Class to hold and print Location object.
 */
class Location {
  private static final Random rand = new Random();
  private final Integer x;
  private final Integer y;

  public Location() {
    int i = rand.nextInt(100);
    if (i <= 75) {
      this.x = (rand.nextInt(14) + 3); //3-17
      this.y = (rand.nextInt(14) + 3);
    } else {
      i = rand.nextInt(2);
      if (i == 0) {
        this.x = rand.nextInt(2) + 1; //1-2
      } else {
        this.x = rand.nextInt(3) + 18; //18-20
      }
      i = rand.nextInt(2);
      if (i == 0) {
        this.y = rand.nextInt(2) + 1;
      } else {
        this.y = rand.nextInt(3) + 18;
      }
    }
  }

  public JSONObject toJSONObject() {
    JSONObject object = new JSONObject();
    object.put("x", x);
    object.put("y", y);
    return object;
  }
}
