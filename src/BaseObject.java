import java.util.Random;

/**
 * Created by Michael Wong and Timothy Chu
 * Base class which holds a Random object.
 */
class BaseObject {
  static final Random random = new Random();
}
