import org.json.JSONObject;

/**
 * Created by Michael Wong and Timothy Chu
 * Class to hold and print Action Regular.
 */
class ActionRegular extends BaseObject {
  private int actionNumber;
  private static final String actionType = "Move";
  private JSONObject location;
  private int pointsAdded;
  private int points;

  public ActionRegular(int actionNumber, int points) {
    this.actionNumber = actionNumber;
    this.location = new Location().toJSONObject();
    this.pointsAdded = random.nextInt(41) - 20;
    this.points = points + pointsAdded;
  }

  public int getActionNumber() {
    return actionNumber;
  }

  public int getPoints() {
    return points;
  }

  public JSONObject toJSONObject() {
    JSONObject object = new JSONObject();
    object.put("actionNumber", actionNumber);
    object.put("actionType", actionType);
    object.put("location", location);
    object.put("pointsAdded", pointsAdded);
    object.put("points", points);
    return object;
  }
}
