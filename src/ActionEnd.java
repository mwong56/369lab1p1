import org.json.JSONObject;

/**
 * Created by Michael Wong and Timothy Chu
 * Class to hold and print Action End.
 */
public class ActionEnd extends BaseObject {

  private final int actionNumber;
  private static final String actionType = "GameEnd";
  private final int points;
  private final GameStatus gameStatus;

  public ActionEnd(int actionNumber, int points) {
    this.actionNumber = actionNumber;
    this.points = points;
    this.gameStatus = random.nextInt(2) == 0 ? GameStatus.Win : GameStatus.Loss;
  }

  public int getActionNumber() {
    return actionNumber;
  }

  public int getPoints() {
    return points;
  }

  public JSONObject toJSONObject() {
    JSONObject toReturn = new JSONObject();
    toReturn.put("actionNumber", actionNumber);
    toReturn.put("actionType", actionType);
    toReturn.put("points", points);
    toReturn.put("gameStatus", gameStatus);
    return toReturn;
  }

  public enum GameStatus {
    Win, Loss
  }
}
